/* 
 * File:   Lentokone.cpp
 * Author: e0380341
 *
 * Created on December 17, 2013, 5:40 PM
 */
#include "Lentokone.h"

Lentokone::Lentokone( int typ, int i ) {
    this->id = i;
    this->tyyppi_id = typ;
    switch( typ ) {
        case 1: // DC-10
            this->tyyppi = "DC-10";
            this->max_lasti = 500;
            this->hinta = 5000000;
            this->istumapaikkoja = 200;
            this->palvelun_taso = 3;
            break;
        case 2: // Boeing 727
            this->tyyppi = "Boeing 727";
            this->max_lasti = 700;
            this->hinta = 22000000;
            this->istumapaikkoja = 300;
            this->palvelun_taso = 4;
            break;
        case 3: // CargoPlane
            this->tyyppi = "CargoPlane";
            this->max_lasti = 10000;
            this->hinta = 4000000;
            this->istumapaikkoja = 12;
            this->palvelun_taso = 1;
    }
}

void Lentokone::TulostaTiedot() {
    cout << " Tyyppi: " << this->tyyppi << endl;
    cout << " Max lasti: " << this->max_lasti << endl;
    cout << " Hinta: " << this->hinta << endl;
    cout << " Istumapaikkoja: " << this->istumapaikkoja << endl;
    cout << " Palvelun taso: " << this->palvelun_taso << endl;
}

string Lentokone::getTyyppi() {
    return this->tyyppi;
}

int Lentokone::getTyyppiId() {
    return this->tyyppi_id;
}

void Lentokone::setLentokentanId( int id ) {
    this->lentokentan_id = id;
}

void Lentokone::setLentoyhtionId( int id ) {
    this->lentoyhtion_id = id;
}

int Lentokone::getLentokentanId() {
    return this->lentokentan_id;
}

int Lentokone::getLentoyhtionId() {
    return this->lentoyhtion_id;
}

int Lentokone::getId() {
    return this->id;
}