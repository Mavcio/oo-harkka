/* 
 * File:   Lentoyhtio.cpp
 * Author: e0380341
 *
 * Created on December 17, 2013, 5:38 PM
 */
#include "Lentoyhtio.h"

Lentoyhtio::Lentoyhtio( string n, int i ) {
    this->nimi = n;
    this->lentokoneet[0] = NULL;
    this->lentokoneita_lkm = 0;
    this->id = i;
}

Lentoyhtio::Lentoyhtio( const Lentoyhtio &obj) {
    cout << "Kopiomuodostin kutsuttu\n";
    // kopioidaan kaikki osoitteet yksitellen
    for( int i = 0; i < lentokoneita_lkm; i++ ) {
        lentokoneet[i] = obj.lentokoneet[i];
    }
}

void Lentoyhtio::TulostaTiedot() {
    cout << this->nimi << endl;
    // tulostetaan jos lentoyhtiö omistaa koneita
    if( lentokoneita_lkm > 0 ) {
        for( int i = 0; i < lentokoneita_lkm; i++ ) {
            cout << i+1 << ")\n";
            lentokoneet[i]->TulostaTiedot();
        }
    }
    else {
        // ei koneita
        cout << " <Ei koneita>\n";
    }
}

void Lentoyhtio::TulostaKoneidenNimet() {
    if( lentokoneita_lkm > 0 ) {
        // tulostetaan kaikkien koneiden nimet
        for( int i = 0; i < lentokoneita_lkm; i++ ) {
            cout << " - " << lentokoneet[i]->getTyyppi() << endl;
        }
    }
    else {
        cout << " <Ei koneita>\n";
    }
}

void Lentoyhtio::TulostaKoneidenNimetNumeroitu() {
    // tulostetaan kaikkien koneiden nimet
    for( int i = 0; i < lentokoneita_lkm; i++ ) {
        cout << i+1 << ") " << lentokoneet[i]->getTyyppi() << endl;
    }
}

string Lentoyhtio::getNimi() {
    return this->nimi;
}

int Lentoyhtio::getKoneenId( int local_id ) {
    return lentokoneet[local_id]->getId();
}

bool Lentoyhtio::OnkoKoneita() {
    return this->lentokoneita_lkm;
}

void Lentoyhtio::LisaaKone( Lentokone *uusi_kone ) {
    uusi_kone->setLentoyhtionId( this->id );
    lentokoneet[lentokoneita_lkm++] = uusi_kone;
}

void Lentoyhtio::PoistaKone( int poistettava_tyyppi ) {
    // käydään kaikki lentoyhtyeen lentokoneet, kunnes poistettava kone löytyy
    for( int i = 0; i < lentokoneita_lkm; i++ ) {
        if( lentokoneet[i]->getTyyppiId() == poistettava_tyyppi ) {
            // korjataan pointterit
            --lentokoneita_lkm;
            for( int j = i; j < lentokoneita_lkm; i++ ) {
                lentokoneet[i] = lentokoneet[i+1];
            }
        }
    }
}