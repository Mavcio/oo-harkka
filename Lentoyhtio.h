/* 
 * File:   Lentoyhtio.h
 * Author: e0380341
 *
 * Created on December 17, 2013, 5:38 PM
 */

#ifndef LENTOYHTIO_H
#define	LENTOYHTIO_H

#include <iostream>
#include "Lentokone.h"
using namespace std;

class Lentoyhtio {
public:
    Lentoyhtio( string, int );
    Lentoyhtio( const Lentoyhtio &obj);
    void TulostaTiedot();
    void TulostaKoneidenNimet();
    void TulostaKoneidenNimetNumeroitu();
    string getNimi();
    bool OnkoKoneita();
    int getKoneenId( int local_id );
    void LisaaKone( Lentokone * );
    void PoistaKone( int );
private:
    string nimi;
    Lentokone *lentokoneet[100];
    int lentokoneita_lkm;
    int id;
};
#endif	/* LENTOYHTIO_H */

