/* 
 * File:   Lentokontrolli.cpp
 * Author: e0380341
 *
 * Created on December 18, 2013, 4:50 PM
 */
#include "Lentokontrolli.h"

Lentokontrolli::Lentokontrolli() {
    lentokoneet_lkm = 0; 
    lentokentat_lkm = 0;
    lentoyhtiot_lkm = 0;
    
    
    // Luodaan kaksi lentokenttää
    lentokentat[lentokentat_lkm] = new Lentokentta( "Helsinki", 3, lentokentat_lkm );
    ++lentokentat_lkm;
    lentokentat[lentokentat_lkm] = new Lentokentta( "Stokis", 30, lentokentat_lkm );
    ++lentokentat_lkm;
    
    // Luodaan kaksi lentoyhtiötä
    lentoyhtiot[lentoyhtiot_lkm] = new Lentoyhtio( "Finnair", lentoyhtiot_lkm );
    ++lentoyhtiot_lkm;
    lentoyhtiot[lentoyhtiot_lkm] = new Lentoyhtio( "KLM", lentoyhtiot_lkm );
    ++lentoyhtiot_lkm;
    
    // Luodaan yksi lentokone
    lentokoneet[lentokoneet_lkm] = new Lentokone( 2, lentokoneet_lkm );
    ++lentokoneet_lkm;
    // Lisätään Hensingin-lentokentälle yksi lentokone
    lentokentat[0]->LisaaKone( lentokoneet[0] );
    // Lisätään sama lentokone Finnairin-listoille
    lentoyhtiot[0]->LisaaKone( lentokoneet[0] );
    
}

Lentokontrolli::~Lentokontrolli() {
    for( int i =0; i < lentokoneet_lkm; i++ )
        delete lentokoneet[i];
    for( int i =0; i < lentokentat_lkm; i++ )
        delete lentokentat[i];
    for( int i =0; i < lentoyhtiot_lkm; i++ )
        delete lentoyhtiot[i];
}

Lentokontrolli::Lentokontrolli( const Lentokontrolli &obj) {
    cout << "Kopiomuodostin kutsuttu\n";
    // kopioidaan kaikki osoitteet yksitellen
    for( int i = 0; i < lentokoneet_lkm; i++ ) {
        lentokoneet[i] = new Lentokone(obj.lentokoneet[i]->getTyyppiId(), i );
        lentokoneet[i] = obj.lentokoneet[i];
    }
    for( int i = 0; i < lentoyhtiot_lkm; i++ ) {
        lentoyhtiot[i] = new Lentoyhtio( obj.lentoyhtiot[i]->getNimi(), i );
        lentoyhtiot[i] = obj.lentoyhtiot[i];
    }
    for( int i = 0; i < lentokentat_lkm; i++ ) {
        lentokentat[i] = new Lentokentta( obj.lentokentat[i]->getNimi(), obj.lentokentat[i]->getMaxKoneita(), i );
        lentokentat[i] = obj.lentokentat[i];
    }
}

void Lentokontrolli::LisaaLentokentta() {
    string nimi, max_koneita_str;
    int max_koneita;
    cout << "Anna lentokentälle nimi: ";
    cin >> nimi;
    cout << "Anna suurin konemäärä: ";
    cin >> max_koneita_str;
    
    // Tarkistetaan onko syöte hyväksyttävä
    max_koneita=atoi(max_koneita_str.c_str());
    if( !(max_koneita) && (max_koneita>=1) ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    lentokentat[lentokentat_lkm] = new Lentokentta( nimi, max_koneita, lentokentat_lkm );
    ++lentokentat_lkm;
}

void Lentokontrolli::ListaaLentokentat() {
    // Tarkistetaan onko lentokenttiä ylipäänsä olemassa
    if( lentokentat_lkm == 0 ) {
        cout << "Ei lentokenttiä olemassa.\n";
        return;
    }
    for( int i = 0; i < lentokentat_lkm; i++ ) {
        cout << i+1 << ") ";
        lentokentat[i]->TulostaTiedot();
    }
}

void Lentokontrolli::TuhoaLentokentta() {
    // Tarkistetaan onko lentokenttiä ylipäänsä olemassa
    if( lentokentat_lkm == 0 ) {
        cout << "Ei lentokenttiä olemassa.\n";
        return;
    }
    string id_str;
    int id = -1;
    // kysytään ensin, mikä lentokenttä tuhotaan
    cout << "Mikä lentokenttä tuhotaan?\n";
    this->ListaaLentokentat();
    cout << "Valintasi: ";
    cin >> id_str;
    
    // Tarkistetaan onko syöte hyväksyttävä
    id = atoi(id_str.c_str());
    if( !id ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    
    // tarkistetaan, onko lentokenttä olemassa
    --id;
    if( (id < lentokentat_lkm) && (id >= 0) ) {
        // Poistetaan lentokentällä olevat koneet
        this->TuhoaLentokentanKoneet( id );
        
        // Poistetaan itse lentokenttä
        Lentokentta *temp = lentokentat[id];
        // korjataan pointterit
        for( int i = id; i < lentokentat_lkm; i++ ) {
            lentokentat[i] = lentokentat[i+1];
        }
        --lentokentat_lkm;
        delete temp;
    }
    else {
        cout << "Virheellinen syöte.\n";
    }
}

void Lentokontrolli::LisaaLentoyhtio() {
    string nimi;
    cout << "Anna lentoyhtiölle nimi: ";
    cin >> nimi;
    
    lentoyhtiot[lentoyhtiot_lkm] = new Lentoyhtio( nimi, lentoyhtiot_lkm );
    ++lentoyhtiot_lkm;
}

void Lentokontrolli::ListaaLentoyhtiot() {
    // Tarkistetaan onko lentoyhtiöitä ylipäänsä olemassa
    if( lentoyhtiot_lkm == 0 ) {
        cout << "Ei lentoyhtiötä olemassa.\n";
        return;
    }
    for( int i = 0; i < lentoyhtiot_lkm; i++ ) {
        lentoyhtiot[i]->TulostaTiedot();
        cout << endl;
    }
}

void Lentokontrolli::TuhoaLentoyhtio() {
    // Tarkistetaan onko lentoyhtiöitä ylipäänsä olemassa
    if( lentoyhtiot_lkm == 0 ) {
        cout << "Ei lentoyhtiötä olemassa.\n";
        return;
    }
    
    string id_str;
    int id = -1;
    // kysytään ensin, mikä lentoyhtio tuhotaan
    cout << "Mikä lentoyhtio tuhotaan?\n";
    for( int i = 0; i < lentoyhtiot_lkm; i++ ) {
        cout << i+1 << ") " << lentoyhtiot[i]->getNimi() << endl;
        lentoyhtiot[i]->TulostaKoneidenNimet();
    }
    cout << "Valintasi: ";
    cin >> id_str;
    
    // Tarkistetaan onko syöte hyväksyttävä
    id = atoi(id_str.c_str());
    if( !id ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    --id;
            
    // tarkistetaan, onko lentokenttä olemassa
    if( (id < lentoyhtiot_lkm) && (id >= 0) ) {
        // Poistetaan lentokentällä olevat koneet
        this->TuhoaLentoyhtionKoneet( id );
        
        // Poistetaan itse lentokenttä
        Lentoyhtio *temp = lentoyhtiot[id];
        // korjataan pointterit
        for( int i = id; i < lentoyhtiot_lkm; i++ ) {
            lentoyhtiot[i] = lentoyhtiot[i+1];
        }
        --lentoyhtiot_lkm;
        delete temp;
    }
    else {
        cout << "Virheellinen syöte!\n";
    }
}

void Lentokontrolli::TuhoaLentokentanKoneet( int lentokentan_id ) {
    Lentokone *temp = NULL;
    
    // Käydään kaikki lentokoneet läpi ja poistetaan kaikki tietyllä lentokentällä olevat koneet
    for( int i = 0; i < lentokoneet_lkm; i++ ) {
        if( lentokoneet[i]->getLentokentanId() == lentokentan_id ) {
            temp = lentokoneet[i];
            // poistetaan kone lentoyhtiöt rekisterista
            this->lentoyhtiot[this->lentokoneet[i]->getLentoyhtionId()]->PoistaKone( this->lentokoneet[i]->getTyyppiId() );
            // korjataan pointterit
            --lentokoneet_lkm;
            for( int j = i; j < lentokoneet_lkm; i++ ) {
                lentokoneet[i] = lentokoneet[i+1];
            }
            delete temp;
        }
    }
}

void Lentokontrolli::TuhoaLentoyhtionKoneet( int lentoyhtion_id) {
    Lentokone *temp = NULL;
    
    // Käydään kaikki lentokoneet läpi ja poistetaan kaikki tietyn lentoyhtiön omistuksessa olevat lentokoneet
    for( int i = 0; i < lentokoneet_lkm; i++ ) {
        if( lentokoneet[i]->getLentoyhtionId() == lentoyhtion_id ) {
            temp = lentokoneet[i];
            // poistetaan kone lentokentän rekisterista
            this->lentokentat[this->lentokoneet[i]->getLentokentanId()]->PoistaKone( this->lentokoneet[i]->getTyyppiId() );
            // korjataan pointterit
            --lentokoneet_lkm;
            for( int j = i; j < lentokoneet_lkm; i++ ) {
                lentokoneet[i] = lentokoneet[i+1];
            }
            delete temp;
        }
    }
}

void Lentokontrolli::LisaaKoneYhtiolle() {
    int lentoyhtio, lentokone, lentokentta;
    string lentoyhtio_str, lentokone_str, lentokentta_str;
    // Tarkistetaan onko lentoyhtiöitä ylipäänsä olemassa
    if( lentoyhtiot_lkm == 0 ) {
        cout << "Ei lentoyhtiötä olemassa.\n";
        return;
    }
    // Tarkistetaan onko lentokenttiä ylipäänsä olemassa
    if( lentokentat_lkm == 0 ) {
        cout << "Ei lentokenttiä olemassa.\n";
        return;
    }
    
    // kysytään ensin mille lentoyhtiölle kone lisätään
    cout << "Valitse lentoyhtiö\n";
    for( int i = 0; i < this->lentoyhtiot_lkm; i++) {
        cout << i+1 << ") " << lentoyhtiot[i]->getNimi() << endl;
    }
    cout << "Valintasi: ";
    cin >> lentoyhtio_str;
    
    // Tarkistetaan onko syöte hyväksyttävä
    lentoyhtio = atoi(lentoyhtio_str.c_str());
    if( !lentoyhtio ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    // tarkistetaan, että syötetty luku vastaa lentoyhtiötä
    if( (lentoyhtio<1) || (lentoyhtio>lentoyhtiot_lkm) ) {
         cout << "Virheellinen syöte!\n";
         return;
    }
    --lentoyhtio;
    
    // mikä lentokone luodaan
    cout << "Valitse lentokone" << endl;
    cout << "1) DC10\n";
    cout << "2) Boeing 727\n";
    cout << "3) CargoPlane\n";
    cout << "Valintasi: ";
    cin >> lentokone_str;
    
    // Tarkistetaan onko syöte hyväksyttävä
    lentokone = atoi(lentokone_str.c_str());
    if( !lentokone ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    // tarkistetaan, että syötetty luku vastaa lentokonetta
    if( (lentokone<1) || (lentokone>3)) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    

    // mille kentälle kone sijoitetaan
    cout << "Mille kentälle kone sijoitetaan?\n";
    for( int i = 0; i < this->lentokentat_lkm; i++ ) {
        cout << i+1 << ") " << lentokentat[i]->getNimi() << endl;
    }
    cout << "Valintasi: ";
    cin >> lentokentta_str;
    
    // Tarkistetaan onko syöte hyväksyttävä
    lentokentta = atoi(lentokentta_str.c_str());
    if( !lentokentta ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    // tarkistetaan, että syötetty luku vastaa lentoyhtiötä
    if( (lentokentta<0) || (lentokentta>lentokentat_lkm) ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    --lentokentta;
    
    // Luodaan nyt uusi lentokone
    lentokoneet[lentokoneet_lkm] = new Lentokone( lentokone, lentokoneet_lkm );
    lentokoneet[lentokoneet_lkm]->setLentokentanId( lentokentta );
    // tarkistetaan, että kentällä on vielä tilaa
    if( !lentokentat[lentokentta]->LisaaKone( lentokoneet[lentokoneet_lkm] ) ) {
        cout << "Lentokentällä ei tilaa. Lentokone tuhotaan.\n";
        delete lentokoneet[lentokoneet_lkm];
        lentokoneet[lentokoneet_lkm] = NULL;
        return;
    }
    lentokoneet[lentokoneet_lkm]->setLentoyhtionId( lentoyhtio );
    lentoyhtiot[lentoyhtio]->LisaaKone( lentokoneet[lentokoneet_lkm] );
    ++lentokoneet_lkm;
}

void Lentokontrolli::TuhoaLentoyhtionKone() {
    // tarkistetaan, että lentoyhtiöitä ylipäänsä olemassa
    if( lentoyhtiot_lkm == 0) {
        cout << "Ei lentoyhtiöitä olemassa!\n";
    }
    int lentoyhtio, tyyppi;;
    string lentoyhtio_str;
    Lentokone *temp = NULL;
    string tyyppi_str;
    
    // ensin käyttäjän tulee valita lentoyhtiö
    cout << "Valitse lentoyhtiö\n";
    for( int i = 0; i < this->lentoyhtiot_lkm; i++) {
        cout << i+1 << ") " << lentoyhtiot[i]->getNimi() << endl;
        lentoyhtiot[i]->TulostaKoneidenNimet();
    }
    cout << "Valintasi: ";
    cin >> lentoyhtio_str;
    
    lentoyhtio = atoi(lentoyhtio_str.c_str());
    if( !lentoyhtio ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    --lentoyhtio;
    
    // sitten kysytään, mikä kone poistetaan
    cout << "Minkä tyyppinen kone tuhotaan?\n";
    cout << "1) DC-10\n";
    cout << "2) Boeing 727\n";
    cout << "3) CargoPlane\n";
    cin >> tyyppi_str;
    tyyppi = atoi(tyyppi_str.c_str());
    if( !tyyppi || tyyppi<1 || tyyppi>3 ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    
    // poistetaan kone ensin yhtiöön rekisterista
    lentoyhtiot[lentoyhtio]->PoistaKone( tyyppi );
    
    // nyt poistetaan se päätietokannasta ja lentokentän listasta
    for( int i = 0; i < lentokoneet_lkm; i++ ) {
        if( (lentokoneet[i]->getLentoyhtionId() == lentoyhtio) 
                && (lentokoneet[i]->getTyyppiId() == tyyppi) ) {
            temp = lentokoneet[i];
            // poistetaan kone lentokentän rekisterista
            this->lentokentat[this->lentokoneet[i]->getLentokentanId()]->PoistaKone( this->lentokoneet[i]->getTyyppiId() );
            // korjataan pointterit
            --lentokoneet_lkm;
            for( int j = i; j < lentokoneet_lkm; i++ ) {
                lentokoneet[i] = lentokoneet[i+1];
            }
            delete temp;
        }
        
    }
}

void Lentokontrolli::LennataLentokonetta() {
    int lentoyhtio, lentokone, lentokentta;
    string lentoyhtio_str, lentokone_str, lentokentta_str;
    
    // minkä lentoyhtiön konetta siirretään
    cout << "Valitse lentoyhtiö\n";
    for( int i = 0; i < this->lentoyhtiot_lkm; i++) {
        cout << i+1 << ") " << lentoyhtiot[i]->getNimi() << endl;
        lentoyhtiot[i]->TulostaKoneidenNimet();
    }
    cout << "Valintasi: ";
    cin >> lentoyhtio_str;
    
    // tarkistetaan, että tuloste on hyväksyttävä
    lentoyhtio = atoi(lentoyhtio_str.c_str());
    if( !lentoyhtio ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    --lentoyhtio;
    if( (lentoyhtio<0) || (lentoyhtio>=lentoyhtiot_lkm) ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    // varmistetaan myös, että lentokentällä koneita
    if( !lentoyhtiot[lentoyhtio]->OnkoKoneita() ) {
        cout << "Lentokentällä ei ole lentokoneita!\n";
        return;
    }
    
    // onko lentoyhtiöllä koneita
    if( !lentoyhtiot[lentoyhtio]->OnkoKoneita() ) {
        cout << "Lentoyhtiöllä ei koneita!.\n";
        return;
    }
    // mikä lentokone siirretään
    cout << "Valitse kone\n";
    lentoyhtiot[lentoyhtio]->TulostaKoneidenNimetNumeroitu();
    cout << "Valintasi: ";
    cin >> lentokone_str;
    lentokone = atoi(lentokone_str.c_str());
    if( !lentokone || lentokone<1 || lentokone>3 ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    --lentokone;
    
    // otetaan selville koneen todellinen id
    lentokone = lentoyhtiot[lentoyhtio]->getKoneenId( lentokone );

    // kohdekenttä:
    cout << "Valitse kohdekenttä:\n";
    for( int i = 0; i < this->lentoyhtiot_lkm; i++ ) {
        cout << i+1 << ") " << lentokentat[i]->getNimi() << endl;
    }
    cout << "Valintasi: ";
    cin >> lentokentta_str;
    
    // tarkistetaan, että tuloste on hyväksyttävä
    lentokentta = atoi(lentokentta_str.c_str());
    if( !lentokentta ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    --lentokentta;
    if( (lentokentta<0) || (lentokentta>=lentoyhtiot_lkm) ) {
        cout << "Virheellinen syöte!\n";
        return;
    }
    
    // tarkistetaan, että kohde kentällä on tilaa
    if( !lentokentat[lentokentta]->LisaaKone( lentokoneet[lentokone] )) {
        cout << "Lentokentällä ei ole tilaa! Kone tuhotaan";
        return;
    }
    // poistetaan kone lähtökentän rekisteristä ja listään kohteeseen
    lentokentat[lentokoneet[lentokone]->getLentokentanId()]->PoistaKone( lentokoneet[lentokone]->getTyyppiId() );
}

void Lentokontrolli::TulostaLentokoneet() {
    for( int i = 0; i < lentokoneet_lkm; i++ )
        cout << i << ": " << lentokoneet[i]->getTyyppi() << endl;
}