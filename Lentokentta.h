/* 
 * File:   Lentokentta.h
 * Author: e0380341
 *
 * Created on December 17, 2013, 5:39 PM
 */

#ifndef LENTOKENTTA_H
#define	LENTOKENTTA_H

#include <iostream>
#include "Lentokone.h"
using namespace std;

class Lentokentta {
public:
    Lentokentta( string, int, int );
    Lentokentta( const Lentokentta &obj);
    void TulostaTiedot();
    string getNimi();
    int getMaxKoneita();
    bool LisaaKone( Lentokone * );
    void PoistaKone( int );
private:
    string nimi;
    int max_koneita;
    Lentokone *lentokoneet[100];
    int lentokone_lkm;
    int id;
};

#endif	/* LENTOKENTTA_H */