/* 
 * File:   Lentokontrolli.h
 * Author: e0380341
 *
 * Created on December 18, 2013, 4:49 PM
 */

#ifndef LENTOKONTROLLI_H
#define	LENTOKONTROLLI_H

#include "Lentokentta.h"
#include "Lentokone.h"
#include "Lentoyhtio.h"
#include <cstdlib>
#include <iostream>
using namespace std;

class Lentokontrolli {
public:
    Lentokontrolli();
    ~Lentokontrolli();
    Lentokontrolli( const Lentokontrolli &obj);
    void ListaaLentokentat();
    void LisaaLentokentta();
    void TuhoaLentokentta();
    void LisaaLentoyhtio();
    void ListaaLentoyhtiot();
    void TuhoaLentoyhtio();
    void LisaaKoneYhtiolle();
    void TuhoaLentoyhtionKone();
    void LennataLentokonetta();
    void TulostaLentokoneet(); // !!!!!!!!!!!!
private:
    void TuhoaLentokentanKoneet( int );
    void TuhoaLentoyhtionKoneet( int );
    Lentokone *lentokoneet[1000];
    int lentokoneet_lkm;
    Lentokentta *lentokentat[100];
    int lentokentat_lkm;
    Lentoyhtio *lentoyhtiot[100];
    int lentoyhtiot_lkm;
};
#endif	/* LENTOKONTROLLI_H */

