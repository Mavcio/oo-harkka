/* 
 * File:   Lentokentta.cpp
 * Author: e0380341
 *
 * Created on December 17, 2013, 5:39 PM
 */
#include "Lentokentta.h"

Lentokentta::Lentokentta( string n, int mk, int i ) {
    this->nimi = n;
    this->max_koneita = mk;
    this->lentokone_lkm = 0;
    this->id = i;
}

Lentokentta::Lentokentta( const Lentokentta &obj) {
    cout << "Kopiomuodostin kutsuttu\n";
    // kopioidaan kaikki osoitteet yksitellen
    for( int i = 0; i < lentokone_lkm; i++ ) {
        lentokoneet[i] = obj.lentokoneet[i];
    }
}

void Lentokentta::TulostaTiedot() {
    cout << this->nimi << ", jolle mahtuu " << this->max_koneita << " konetta.\n";
    if( lentokone_lkm > 0 ) {
        // tulostetaan lentokentällä seisovat lentokoneet
        for( int i = 0; i < lentokone_lkm; i++ ) {
            cout << " - " << lentokoneet[i]->getTyyppi() << endl;
        } // end for
    } // end if
    else {
        // lentokenttä onkin tyhjä
        cout << " <Ei koneita>\n";
    } // end else
} // end TulostaTiedot()

string Lentokentta::getNimi() {
    return this->nimi;
}

int Lentokentta::getMaxKoneita() {
    return this->max_koneita;
}

bool Lentokentta::LisaaKone( Lentokone *uusi_kone ) {
    // tarkistetaan, että lentokentällä on vielä tilaa
    if( lentokone_lkm >= max_koneita ) {
        return false;
    } // end if
    // vielä on tilaa
    else {
        uusi_kone->setLentokentanId( this->id );
        lentokoneet[lentokone_lkm++] = uusi_kone;
        return true;
    } // end else
} // end LisaaKone


void Lentokentta::PoistaKone( int poistettava_tyyppi ) {
    // käydään kaikki lentoyhtyeen lentokoneet, kunnes poistettava kone löytyy
    for( int i = 0; i < lentokone_lkm; i++ ) {
        if( lentokoneet[i]->getTyyppiId() == poistettava_tyyppi ) {
            // korjataan pointterit
            --lentokone_lkm;
            for( int j = i; j < lentokone_lkm; i++ ) {
                lentokoneet[i] = lentokoneet[i+1];
            }
        }
    }
}