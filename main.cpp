/* 
 * File:   main.cpp
 * Author: e0380341, Aleksander Khlebnikov
 *
 * Created on December 17, 2013, 4:42 PM
 * 
 * Sisältää ohjelman logiikan
 */

/*
 Lähdekooditiedoston alkuun tulisi kommentoida seuraavat asiat
–Ohjelman, tiedoston ja tekijän nimet
– Kuvaus tiedoston sisältämistä asioista
– Kehitysympäristö
– Muutoshistoria
–Lisenssi

Kunkin funktion alkuun tulisi kommentoida seuraavat asiat
– Funktion nimi
– Kuvaus funktiosta
– Funktioon tuleva tieto
– Funktiosta lähtevä tieto
 */
#include <iostream>
#include <cstdlib>
#include "Lentokontrolli.h"
using namespace std;

int Valikko();

int main(int argc, char** argv) {
    Lentokontrolli kontrolli;
    bool lopeta = false;
    
    while( !lopeta ) {
        switch( Valikko() ) {
            case 1: // Listaa lentokentät ja siellä seisovat koneet
                kontrolli.ListaaLentokentat();
                break;
            case 2: // Lisää lentokenttä
                kontrolli.LisaaLentokentta();
                break;
            case 3: // Tuhoa lentokenttä
                kontrolli.TuhoaLentokentta();
                break;
            case 4: // Listaa lentoyhtiöt ja niiden koneet
                kontrolli.ListaaLentoyhtiot();
                break;
            case 5: // Lisää lentoyhtiö
                kontrolli.LisaaLentoyhtio();
                break;
            case 6: // Tuhoa lentoyhtiö
                kontrolli.TuhoaLentoyhtio();
                break;
            case 7: // Lisää lentokone lentoyhtiölle
                kontrolli.LisaaKoneYhtiolle();
                break;
            case 8: // Tuhoa lentokone lentoyhtiöltä
                kontrolli.TuhoaLentoyhtionKone();
                break;
            case 9: // Lennätä lentokonetta kentältä toiselle
                kontrolli.LennataLentokonetta();
                break;
            case 0:
                lopeta = true;
                break;
            default:
                cout << "Virheellinen syöte!\n";
                break;
        }
    }

    return 0;
}

int Valikko() {
    string valinta_str;
    int valinta = -1;
    
    // Otetaan käyttäjän valinta 
    cout << "\n**Valikko**" << endl;
    cout << "1) Listaa lentokentät ja siellä seisovat koneet" << endl;
    cout << "2) Lisää lentokenttä" << endl;
    cout << "3) Tuhoa lentokenttä" << endl;
    cout << "4) Listaa lentoyhtiöt ja niiden koneet" << endl;
    cout << "5) Lisää lentoyhtiö" << endl;
    cout << "6) Tuhoa lentoyhtiö" << endl;
    cout << "7) Lisää lentokone lentoyhtiölle" << endl;
    cout << "8) Tuhoa lentokone lentoyhtiöltä" << endl;
    cout << "9) Lennätä lentokonetta kentältä toiselle" << endl;
    cout << "0) Lopeta" << endl;
    cout << "Valintasi: ";
    cin >> valinta_str;
    // tarkistetaan syötteen oikeellisuus
    if( (valinta_str.length()==1) && isdigit(valinta_str.c_str()[0]) ) {
        valinta = atoi( valinta_str.c_str() );
    }
    else 
        valinta = -1;
    cout << endl;
    
    return valinta;
} // end Valikko()