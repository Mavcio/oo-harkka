/* 
 * File:   Lentokone.h
 * Author: e0380341
 *
 * Created on December 17, 2013, 5:40 PM
 */

#ifndef LENTOKONE_H
#define	LENTOKONE_H

#include <iostream>
using namespace std;

class Lentokone {
public:
    Lentokone( int, int );
    void TulostaTiedot();
    string getTyyppi();
    int getTyyppiId();
    void setLentokentanId( int );
    void setLentoyhtionId( int );
    int getId();
    int getLentokentanId();
    int getLentoyhtionId();
private:
    string tyyppi;
    int max_lasti;
    int hinta;
    int istumapaikkoja;
    int palvelun_taso;
    int tyyppi_id;
    int id;
    int lentokentan_id;
    int lentoyhtion_id;
};
#endif	/* LENTOKONE_H */
